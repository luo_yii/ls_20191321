/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : documentdb

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_group
-- ----------------------------
INSERT INTO `auth_group` VALUES (4, '游客');
INSERT INTO `auth_group` VALUES (3, '用户');
INSERT INTO `auth_group` VALUES (2, '管理员');
INSERT INTO `auth_group` VALUES (1, '超级管理员');

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `group_id` int(0) NOT NULL,
  `permission_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id`, `permission_id`) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------
INSERT INTO `auth_group_permissions` VALUES (1, 1, 1);
INSERT INTO `auth_group_permissions` VALUES (2, 1, 2);
INSERT INTO `auth_group_permissions` VALUES (3, 1, 3);
INSERT INTO `auth_group_permissions` VALUES (4, 1, 4);
INSERT INTO `auth_group_permissions` VALUES (5, 1, 5);
INSERT INTO `auth_group_permissions` VALUES (6, 1, 6);
INSERT INTO `auth_group_permissions` VALUES (7, 1, 7);
INSERT INTO `auth_group_permissions` VALUES (8, 1, 8);
INSERT INTO `auth_group_permissions` VALUES (9, 1, 9);
INSERT INTO `auth_group_permissions` VALUES (10, 1, 10);
INSERT INTO `auth_group_permissions` VALUES (11, 1, 11);
INSERT INTO `auth_group_permissions` VALUES (12, 1, 12);
INSERT INTO `auth_group_permissions` VALUES (13, 1, 13);
INSERT INTO `auth_group_permissions` VALUES (14, 1, 14);
INSERT INTO `auth_group_permissions` VALUES (15, 1, 15);
INSERT INTO `auth_group_permissions` VALUES (16, 1, 16);
INSERT INTO `auth_group_permissions` VALUES (17, 1, 17);
INSERT INTO `auth_group_permissions` VALUES (18, 1, 18);
INSERT INTO `auth_group_permissions` VALUES (19, 1, 19);
INSERT INTO `auth_group_permissions` VALUES (20, 1, 20);
INSERT INTO `auth_group_permissions` VALUES (21, 1, 21);
INSERT INTO `auth_group_permissions` VALUES (22, 1, 22);
INSERT INTO `auth_group_permissions` VALUES (23, 1, 23);
INSERT INTO `auth_group_permissions` VALUES (24, 1, 24);
INSERT INTO `auth_group_permissions` VALUES (25, 1, 25);
INSERT INTO `auth_group_permissions` VALUES (26, 1, 26);
INSERT INTO `auth_group_permissions` VALUES (27, 1, 27);
INSERT INTO `auth_group_permissions` VALUES (28, 1, 28);
INSERT INTO `auth_group_permissions` VALUES (29, 1, 29);
INSERT INTO `auth_group_permissions` VALUES (30, 1, 30);
INSERT INTO `auth_group_permissions` VALUES (31, 1, 31);
INSERT INTO `auth_group_permissions` VALUES (32, 1, 32);
INSERT INTO `auth_group_permissions` VALUES (33, 1, 33);
INSERT INTO `auth_group_permissions` VALUES (34, 1, 34);
INSERT INTO `auth_group_permissions` VALUES (35, 1, 35);
INSERT INTO `auth_group_permissions` VALUES (36, 1, 36);
INSERT INTO `auth_group_permissions` VALUES (37, 1, 37);
INSERT INTO `auth_group_permissions` VALUES (38, 1, 38);
INSERT INTO `auth_group_permissions` VALUES (39, 1, 39);
INSERT INTO `auth_group_permissions` VALUES (40, 1, 40);
INSERT INTO `auth_group_permissions` VALUES (41, 2, 1);
INSERT INTO `auth_group_permissions` VALUES (42, 2, 5);
INSERT INTO `auth_group_permissions` VALUES (43, 2, 9);
INSERT INTO `auth_group_permissions` VALUES (44, 2, 13);
INSERT INTO `auth_group_permissions` VALUES (45, 2, 17);
INSERT INTO `auth_group_permissions` VALUES (46, 2, 20);
INSERT INTO `auth_group_permissions` VALUES (47, 2, 21);
INSERT INTO `auth_group_permissions` VALUES (48, 2, 22);
INSERT INTO `auth_group_permissions` VALUES (49, 2, 23);
INSERT INTO `auth_group_permissions` VALUES (50, 2, 24);
INSERT INTO `auth_group_permissions` VALUES (51, 2, 25);
INSERT INTO `auth_group_permissions` VALUES (52, 2, 26);
INSERT INTO `auth_group_permissions` VALUES (53, 2, 27);
INSERT INTO `auth_group_permissions` VALUES (54, 2, 28);
INSERT INTO `auth_group_permissions` VALUES (55, 2, 29);
INSERT INTO `auth_group_permissions` VALUES (56, 2, 30);
INSERT INTO `auth_group_permissions` VALUES (57, 2, 31);
INSERT INTO `auth_group_permissions` VALUES (58, 2, 32);
INSERT INTO `auth_group_permissions` VALUES (59, 2, 33);
INSERT INTO `auth_group_permissions` VALUES (60, 2, 34);
INSERT INTO `auth_group_permissions` VALUES (61, 2, 35);
INSERT INTO `auth_group_permissions` VALUES (62, 2, 36);
INSERT INTO `auth_group_permissions` VALUES (63, 2, 37);
INSERT INTO `auth_group_permissions` VALUES (64, 2, 40);
INSERT INTO `auth_group_permissions` VALUES (65, 3, 24);
INSERT INTO `auth_group_permissions` VALUES (68, 3, 28);
INSERT INTO `auth_group_permissions` VALUES (66, 3, 32);
INSERT INTO `auth_group_permissions` VALUES (67, 3, 36);

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `content_type_id` int(0) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id`, `codename`) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO `auth_permission` VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO `auth_permission` VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO `auth_permission` VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO `auth_permission` VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO `auth_permission` VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO `auth_permission` VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO `auth_permission` VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO `auth_permission` VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO `auth_permission` VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO `auth_permission` VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO `auth_permission` VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO `auth_permission` VALUES (13, 'Can add content type', 4, 'add_contenttype');
INSERT INTO `auth_permission` VALUES (14, 'Can change content type', 4, 'change_contenttype');
INSERT INTO `auth_permission` VALUES (15, 'Can delete content type', 4, 'delete_contenttype');
INSERT INTO `auth_permission` VALUES (16, 'Can view content type', 4, 'view_contenttype');
INSERT INTO `auth_permission` VALUES (17, 'Can add session', 5, 'add_session');
INSERT INTO `auth_permission` VALUES (18, 'Can change session', 5, 'change_session');
INSERT INTO `auth_permission` VALUES (19, 'Can delete session', 5, 'delete_session');
INSERT INTO `auth_permission` VALUES (20, 'Can view session', 5, 'view_session');
INSERT INTO `auth_permission` VALUES (21, 'Can add 公文信息', 6, 'add_document');
INSERT INTO `auth_permission` VALUES (22, 'Can change 公文信息', 6, 'change_document');
INSERT INTO `auth_permission` VALUES (23, 'Can delete 公文信息', 6, 'delete_document');
INSERT INTO `auth_permission` VALUES (24, 'Can view 公文信息', 6, 'view_document');
INSERT INTO `auth_permission` VALUES (25, 'Can add 公文分类', 7, 'add_label');
INSERT INTO `auth_permission` VALUES (26, 'Can change 公文分类', 7, 'change_label');
INSERT INTO `auth_permission` VALUES (27, 'Can delete 公文分类', 7, 'delete_label');
INSERT INTO `auth_permission` VALUES (28, 'Can view 公文分类', 7, 'view_label');
INSERT INTO `auth_permission` VALUES (29, 'Can add 公文动态', 8, 'add_dynamic');
INSERT INTO `auth_permission` VALUES (30, 'Can change 公文动态', 8, 'change_dynamic');
INSERT INTO `auth_permission` VALUES (31, 'Can delete 公文动态', 8, 'delete_dynamic');
INSERT INTO `auth_permission` VALUES (32, 'Can view 公文动态', 8, 'view_dynamic');
INSERT INTO `auth_permission` VALUES (33, 'Can add 公文批复', 9, 'add_comment');
INSERT INTO `auth_permission` VALUES (34, 'Can change 公文批复', 9, 'change_comment');
INSERT INTO `auth_permission` VALUES (35, 'Can delete 公文批复', 9, 'delete_comment');
INSERT INTO `auth_permission` VALUES (36, 'Can view 公文批复', 9, 'view_comment');
INSERT INTO `auth_permission` VALUES (37, 'Can add user', 10, 'add_myuser');
INSERT INTO `auth_permission` VALUES (38, 'Can change user', 10, 'change_myuser');
INSERT INTO `auth_permission` VALUES (39, 'Can delete user', 10, 'delete_myuser');
INSERT INTO `auth_permission` VALUES (40, 'Can view user', 10, 'view_myuser');

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `object_repr` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `action_flag` smallint(0) UNSIGNED NOT NULL,
  `change_message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `content_type_id` int(0) NULL DEFAULT NULL,
  `user_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co`(`content_type_id`) USING BTREE,
  INDEX `django_admin_log_user_id_c564eba6_fk_user_myuser_id`(`user_id`) USING BTREE,
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_user_myuser_id` FOREIGN KEY (`user_id`) REFERENCES `user_myuser` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO `django_admin_log` VALUES (1, '2021-06-02 09:52:53.156697', '15', '团队作业', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (2, '2021-06-02 09:52:53.159613', '16', 'test', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (3, '2021-06-02 09:52:53.163615', '17', 'test', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (4, '2021-06-02 09:52:53.166525', '18', 'test', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (5, '2021-06-02 09:52:53.168535', '19', 'test', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (6, '2021-06-02 09:52:53.170534', '20', 'test', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (7, '2021-06-02 09:57:25.683513', '21', 'test', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (8, '2021-06-02 09:58:54.683538', '15', 'Dynamic object (15)', 3, '', 8, 1);
INSERT INTO `django_admin_log` VALUES (9, '2021-06-04 12:38:51.790595', '1', '超级管理员', 1, '[{\"added\": {}}]', 3, 1);
INSERT INTO `django_admin_log` VALUES (10, '2021-06-04 12:41:31.516571', '2', '管理员', 1, '[{\"added\": {}}]', 3, 1);
INSERT INTO `django_admin_log` VALUES (11, '2021-06-04 12:42:12.236062', '3', '用户', 1, '[{\"added\": {}}]', 3, 1);
INSERT INTO `django_admin_log` VALUES (12, '2021-06-04 16:37:51.263851', '11', 'user10', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (13, '2021-06-04 16:37:51.280779', '12', 'user11', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (14, '2021-06-04 16:37:51.284773', '13', 'user12', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (15, '2021-06-04 16:37:51.289774', '14', 'user13', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (16, '2021-06-04 16:37:51.293821', '3', 'user2', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (17, '2021-06-04 16:37:51.297781', '4', 'user3', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (18, '2021-06-04 16:37:51.301826', '5', 'user4', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (19, '2021-06-04 16:37:51.305824', '6', 'user5', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (20, '2021-06-04 16:37:51.309817', '7', 'user6', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (21, '2021-06-04 16:37:51.313820', '8', 'user7', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (22, '2021-06-04 16:37:51.317815', '9', 'user8', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (23, '2021-06-04 16:37:51.321822', '10', 'user9', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (24, '2021-06-04 16:38:43.844736', '4', '游客', 1, '[{\"added\": {}}]', 3, 1);
INSERT INTO `django_admin_log` VALUES (25, '2021-06-04 16:39:54.923493', '1', '关于申请灾后图书馆新书购置资金的请示', 2, '[{\"changed\": {\"fields\": [\"person\"]}}]', 6, 1);
INSERT INTO `django_admin_log` VALUES (26, '2021-06-04 16:40:05.079727', '1', '关于申请灾后图书馆新书购置资金的请示', 2, '[{\"changed\": {\"fields\": [\"person\"]}}]', 6, 1);
INSERT INTO `django_admin_log` VALUES (27, '2021-06-05 02:49:18.517209', '2', 'user1', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (28, '2021-06-05 03:00:42.527183', '15', 'user1', 3, '', 10, 1);
INSERT INTO `django_admin_log` VALUES (29, '2021-06-05 09:04:29.495345', '23', 'test_en', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (30, '2021-06-05 09:04:29.510167', '24', 'test_en', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (31, '2021-06-05 09:12:38.618420', '25', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (32, '2021-06-05 09:12:38.634022', '26', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (33, '2021-06-05 09:17:31.644850', '27', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (34, '2021-06-05 09:17:31.671764', '28', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (35, '2021-06-05 09:18:19.839635', '29', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (36, '2021-06-05 09:22:52.050414', '30', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (37, '2021-06-05 09:32:00.585550', '31', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (38, '2021-06-05 09:32:00.599918', '32', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (39, '2021-06-05 09:46:23.031314', '33', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (40, '2021-06-05 09:49:45.089527', '34', 'testen', 3, '', 6, 1);
INSERT INTO `django_admin_log` VALUES (41, '2021-06-05 11:04:00.167650', '35', 'testen', 3, '', 6, 1);

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label`, `model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES (1, 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES (3, 'auth', 'group');
INSERT INTO `django_content_type` VALUES (2, 'auth', 'permission');
INSERT INTO `django_content_type` VALUES (4, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES (9, 'index', 'comment');
INSERT INTO `django_content_type` VALUES (6, 'index', 'document');
INSERT INTO `django_content_type` VALUES (8, 'index', 'dynamic');
INSERT INTO `django_content_type` VALUES (7, 'index', 'label');
INSERT INTO `django_content_type` VALUES (5, 'sessions', 'session');
INSERT INTO `django_content_type` VALUES (10, 'user', 'myuser');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2021-06-01 02:09:32.279874');
INSERT INTO `django_migrations` VALUES (2, 'contenttypes', '0002_remove_content_type_name', '2021-06-01 02:09:32.357430');
INSERT INTO `django_migrations` VALUES (3, 'auth', '0001_initial', '2021-06-01 02:09:32.427569');
INSERT INTO `django_migrations` VALUES (4, 'auth', '0002_alter_permission_name_max_length', '2021-06-01 02:09:32.674609');
INSERT INTO `django_migrations` VALUES (5, 'auth', '0003_alter_user_email_max_length', '2021-06-01 02:09:32.680633');
INSERT INTO `django_migrations` VALUES (6, 'auth', '0004_alter_user_username_opts', '2021-06-01 02:09:32.687022');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0005_alter_user_last_login_null', '2021-06-01 02:09:32.693030');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0006_require_contenttypes_0002', '2021-06-01 02:09:32.697324');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0007_alter_validators_add_error_messages', '2021-06-01 02:09:32.705289');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0008_alter_user_username_max_length', '2021-06-01 02:09:32.720287');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0009_alter_user_last_name_max_length', '2021-06-01 02:09:32.727026');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0010_alter_group_name_max_length', '2021-06-01 02:09:32.743993');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0011_update_proxy_permissions', '2021-06-01 02:09:32.750907');
INSERT INTO `django_migrations` VALUES (14, 'user', '0001_initial', '2021-06-01 02:09:32.841467');
INSERT INTO `django_migrations` VALUES (15, 'admin', '0001_initial', '2021-06-01 02:09:33.098137');
INSERT INTO `django_migrations` VALUES (16, 'admin', '0002_logentry_remove_auto_add', '2021-06-01 02:09:33.195834');
INSERT INTO `django_migrations` VALUES (17, 'admin', '0003_logentry_add_action_flag_choices', '2021-06-01 02:09:33.204437');
INSERT INTO `django_migrations` VALUES (18, 'index', '0001_initial', '2021-06-01 02:09:33.297072');
INSERT INTO `django_migrations` VALUES (19, 'sessions', '0001_initial', '2021-06-01 02:09:33.438899');
INSERT INTO `django_migrations` VALUES (20, 'index', '0002_auto_20210601_1010', '2021-06-01 02:10:48.275784');
INSERT INTO `django_migrations` VALUES (21, 'index', '0003_document_type', '2021-06-01 11:20:34.063280');
INSERT INTO `django_migrations` VALUES (22, 'index', '0004_document_lyrics', '2021-06-02 01:52:09.961916');
INSERT INTO `django_migrations` VALUES (23, 'index', '0005_auto_20210602_0952', '2021-06-02 01:52:50.222556');
INSERT INTO `django_migrations` VALUES (24, 'index', '0006_auto_20210602_0953', '2021-06-02 01:53:50.719091');
INSERT INTO `django_migrations` VALUES (25, 'index', '0007_document_key', '2021-06-05 09:43:46.016275');
INSERT INTO `django_migrations` VALUES (26, 'user', '0002_auto_20210605_1743', '2021-06-05 09:43:46.034598');
INSERT INTO `django_migrations` VALUES (27, 'index', '0008_auto_20210605_1920', '2021-06-05 11:20:46.924735');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `session_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('9pjhxahthmwwnaqse59dc5sv4z4n35a3', 'NjU0YTE2MDg0NDFkODk2NmY1ZjYxNTRlYTM0YTJhY2YzYzQ4MTczNjp7InBsYXlfbGlzdCI6W3siaWQiOjEsInNpbmdlciI6Ilx1NWYyMFx1NGUwOSIsIm5hbWUiOiJcdTUxNzNcdTRlOGVcdTc1MzNcdThiZjdcdTcwN2VcdTU0MGVcdTU2ZmVcdTRlNjZcdTk5ODZcdTY1YjBcdTRlNjZcdThkMmRcdTdmNmVcdThkNDRcdTkxZDFcdTc2ODRcdThiZjdcdTc5M2EifV19', '2021-06-15 14:26:08.228159');
INSERT INTO `django_session` VALUES ('chw33x98gb6g2kiaolntwi5ki5izyrnk', 'NDY4OWUzMWUwNWU4YzliYjg1OWJiNjM3MmQ0ZWJlMWI4Mzg1YWRiZTp7fQ==', '2021-06-18 16:55:51.824918');
INSERT INTO `django_session` VALUES ('p5bify9oz8fsr0atu42trt98mg4k6cav', 'YTVmZDk3MzIwNGQwNmI0ZWIwYmJkMmEwMGQwZDQ1MjFkYTliYzdkZDp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYWI1YmQ4YmQzMWVmZGU5ZmJiMjBhNDBhMjdhYTY1Y2Q0NWFiMjQ1NyIsInBsYXlfbGlzdCI6W3siaWQiOjQsInNpbmdlciI6Ilx1Njc0ZVx1NTZkYiIsIm5hbWUiOiJYXHU3NzAxWFx1NTNiZlx1NjU1OVx1ODBiMlx1NWM0MFx1NTE3M1x1NGU4ZVx1NTQwY1x1NjEwZlx1NWYwMFx1NWM1NVx1NjY5MVx1NjcxZlx1NjUyZlx1NjU1OVx1NmQzYlx1NTJhOFx1NzY4NFx1NTFmZCJ9XX0=', '2021-06-19 13:11:40.110828');
INSERT INTO `django_session` VALUES ('rlom1iped40pephfk0xe1wca1nuk8vmv', 'NjU0YTE2MDg0NDFkODk2NmY1ZjYxNTRlYTM0YTJhY2YzYzQ4MTczNjp7InBsYXlfbGlzdCI6W3siaWQiOjEsInNpbmdlciI6Ilx1NWYyMFx1NGUwOSIsIm5hbWUiOiJcdTUxNzNcdTRlOGVcdTc1MzNcdThiZjdcdTcwN2VcdTU0MGVcdTU2ZmVcdTRlNjZcdTk5ODZcdTY1YjBcdTRlNjZcdThkMmRcdTdmNmVcdThkNDRcdTkxZDFcdTc2ODRcdThiZjdcdTc5M2EifV19', '2021-06-15 14:59:28.824650');

-- ----------------------------
-- Table structure for index_comment
-- ----------------------------
DROP TABLE IF EXISTS `index_comment`;
CREATE TABLE `index_comment`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `text` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date` date NOT NULL,
  `document_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_comment_document_id_5be8c09b_fk_index_document_id`(`document_id`) USING BTREE,
  CONSTRAINT `index_comment_document_id_5be8c09b_fk_index_document_id` FOREIGN KEY (`document_id`) REFERENCES `index_document` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of index_comment
-- ----------------------------
INSERT INTO `index_comment` VALUES (1, '同意', 'admin', '2021-06-02', 1);

-- ----------------------------
-- Table structure for index_document
-- ----------------------------
DROP TABLE IF EXISTS `index_document`;
CREATE TABLE `index_document`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `office` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `person` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `Classification` tinyint(1) NOT NULL,
  `time` date NOT NULL,
  `img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `file` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `label_id` int(0) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `lyrics` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `key` longblob NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_document_label_id_b5ce761f_fk_index_label_id`(`label_id`) USING BTREE,
  CONSTRAINT `index_document_label_id_b5ce761f_fk_index_label_id` FOREIGN KEY (`label_id`) REFERENCES `index_label` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of index_document
-- ----------------------------
-- ----------------------------
-- Table structure for index_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `index_dynamic`;
CREATE TABLE `index_dynamic`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `plays` int(0) NOT NULL,
  `search` int(0) NOT NULL,
  `download` int(0) NOT NULL,
  `document_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_dynamic_document_id_dc991234_fk_index_document_id`(`document_id`) USING BTREE,
  CONSTRAINT `index_dynamic_document_id_dc991234_fk_index_document_id` FOREIGN KEY (`document_id`) REFERENCES `index_document` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of index_dynamic
-- ----------------------------
INSERT INTO `index_dynamic` VALUES (1, 69, 32, 11, 1);
INSERT INTO `index_dynamic` VALUES (2, 7, 0, 0, 2);
INSERT INTO `index_dynamic` VALUES (3, 30, 0, 1, 3);
INSERT INTO `index_dynamic` VALUES (4, 50, 0, 0, 4);
INSERT INTO `index_dynamic` VALUES (5, 0, 0, 0, 5);
INSERT INTO `index_dynamic` VALUES (6, 0, 0, 0, 6);
INSERT INTO `index_dynamic` VALUES (7, 1, 0, 0, 7);
INSERT INTO `index_dynamic` VALUES (8, 0, 0, 0, 8);
INSERT INTO `index_dynamic` VALUES (9, 0, 0, 0, 9);
INSERT INTO `index_dynamic` VALUES (10, 0, 0, 0, 10);
INSERT INTO `index_dynamic` VALUES (11, 0, 0, 0, 11);
INSERT INTO `index_dynamic` VALUES (12, 0, 0, 0, 12);
INSERT INTO `index_dynamic` VALUES (13, 0, 0, 0, 13);
INSERT INTO `index_dynamic` VALUES (14, 0, 0, 0, 14);
INSERT INTO `index_dynamic` VALUES (16, 1, 0, 1, 22);
INSERT INTO `index_dynamic` VALUES (30, 4, 0, 4, 36);

-- ----------------------------
-- Table structure for index_label
-- ----------------------------
DROP TABLE IF EXISTS `index_label`;
CREATE TABLE `index_label`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of index_label
-- ----------------------------
INSERT INTO `index_label` VALUES (1, '条例');
INSERT INTO `index_label` VALUES (2, '请示');
INSERT INTO `index_label` VALUES (3, '决定');
INSERT INTO `index_label` VALUES (4, '命令');
INSERT INTO `index_label` VALUES (5, '指示');
INSERT INTO `index_label` VALUES (6, '批复');
INSERT INTO `index_label` VALUES (7, '通知');
INSERT INTO `index_label` VALUES (8, '通报');
INSERT INTO `index_label` VALUES (9, '公告');
INSERT INTO `index_label` VALUES (10, '通告');
INSERT INTO `index_label` VALUES (11, '议案');
INSERT INTO `index_label` VALUES (12, '报告');
INSERT INTO `index_label` VALUES (13, '涵');
INSERT INTO `index_label` VALUES (14, '会议纪要');

-- ----------------------------
-- Table structure for user_myuser
-- ----------------------------
DROP TABLE IF EXISTS `user_myuser`;
CREATE TABLE `user_myuser`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `is_secert` tinyint(1) NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_myuser
-- ----------------------------
INSERT INTO `user_myuser` VALUES (1, 'pbkdf2_sha256$150000$F3Y7G6ljFKfX$vT7WoKqCTgrIdIpvlQwZ8kpgvPXIN0CpTdD25dIe3ik=', '2021-06-05 13:06:03.745857', 1, 'admin', '', '', 1, 1, '2021-06-02 03:47:53.210785', 1, '1619553792@qq.com');
INSERT INTO `user_myuser` VALUES (16, 'pbkdf2_sha256$150000$7QW6PHgOzWsw$8ifO9MEy3OiYF8PT9QMzub2PyaX0PanRFYFi7xUO4MA=', '2021-06-05 13:06:43.485517', 0, 'user1', '', '', 0, 1, '2021-06-05 03:01:05.328921', 0, 'user1@163.com');

-- ----------------------------
-- Table structure for user_myuser_groups
-- ----------------------------
DROP TABLE IF EXISTS `user_myuser_groups`;
CREATE TABLE `user_myuser_groups`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(0) NOT NULL,
  `group_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_myuser_groups_myuser_id_group_id_680fbae2_uniq`(`myuser_id`, `group_id`) USING BTREE,
  INDEX `user_myuser_groups_group_id_e21a6dfd_fk_auth_group_id`(`group_id`) USING BTREE,
  CONSTRAINT `user_myuser_groups_group_id_e21a6dfd_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_myuser_groups_myuser_id_dfd02c0f_fk_user_myuser_id` FOREIGN KEY (`myuser_id`) REFERENCES `user_myuser` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_myuser_groups
-- ----------------------------
INSERT INTO `user_myuser_groups` VALUES (1, 1, 2);
INSERT INTO `user_myuser_groups` VALUES (3, 1, 3);

-- ----------------------------
-- Table structure for user_myuser_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `user_myuser_user_permissions`;
CREATE TABLE `user_myuser_user_permissions`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `myuser_id` int(0) NOT NULL,
  `permission_id` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_myuser_user_permiss_myuser_id_permission_id_ae8df385_uniq`(`myuser_id`, `permission_id`) USING BTREE,
  INDEX `user_myuser_user_per_permission_id_d16c386c_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `user_myuser_user_per_myuser_id_5d2dcfb0_fk_user_myus` FOREIGN KEY (`myuser_id`) REFERENCES `user_myuser` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_myuser_user_per_permission_id_d16c386c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_myuser_user_permissions
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
